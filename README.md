# helix-orientation-lock

This is a daemon that handles screen orientation locking on Lenovo
ThinkPad Helix 2nd Gen (20CG or 20CH) computers running GNOME 3 on
GNU/Linux.

The primary functionality is to enable the "rotation lock" button on
the device (next to the power button).  Out of the box, this button
does nothing in GNU/Linux.  This daemon allows you to use this button
to toggle the GNOME screen orientation-lock setting.  Optionally, the
software can also automatically lock the screen orientation when
docking the device and unlock it when undocking to tablet mode.

## Requirements

* a Lenovo ThinkPad Helix 2nd Gen (20CG or 20CH) running GNOME 3 on
  GNU/Linux.  1st Gen hardware hasn't been tested.
* auto-rotation correctly set up via
  [iio-sensor-proxy](https://github.com/hadess/iio-sensor-proxy).  If
  you are running the Linux kernel version 4.14 or higher, see the
  [Arch Linux
  wiki](https://wiki.archlinux.org/index.php/Lenovo_ThinkPad_Helix_2nd_Gen#Sensors)
  for information on how to get this working.
* `gio` (part of `glib`), `libevdev` and `libnotify` libraries.  These
  are probably already installed on any GNOME system.  You need
  `pkg-config` installed for building the software from source.

## Configuration & Installation

If you've gotten this code from the repository on
[GitLab](https://gitlab.com/brandoninvergo/tp-helix-orientation-lock),
you first need to run `autoreconf -fvi` (requires Autoconf and
Automake to be installed).

As usual, run `./configure`, `make` and `make install` to configure,
build and install the software.  If you want to install the systemd
service file, run `./configure --with-systemd`.

## Usage

### Set up permissions

Check which group the files in `/dev/input/` belong to:

    $ ls -l /dev/input | head
    total 0K
    drwxr-xr-x 2 root root      1K May  3 09:21 by-id
    drwxr-xr-x 2 root root      1K May  3 09:21 by-path
    crw-rw---- 1 root input 13, 64 May  3 09:17 event0
    crw-rw---- 1 root input 13, 65 May  3 09:17 event1
    crw-rw---- 1 root input 13, 74 May  3 09:17 event10
    crw-rw---- 1 root input 13, 75 May  3 09:17 event11
    crw-rw---- 1 root input 13, 76 May  3 09:17 event12
    crw-rw---- 1 root input 13, 77 May  3 09:17 event13
    crw-rw---- 1 root input 13, 78 May  3 09:17 event14

Here they belong to the `input` group.  So, to use `helix-orientation-lock` as a
user, you must add the user to the `input` group:

    # gpasswd --add myusername input

### Run the program

Simply run `helix-orientation-lock` to start the program.  Test the
program by hitting the rotation lock button.  You can quickly check
the current status by opening the GNOME user menu at the top right of
the screen and looking at the icon in the orientation lock menu button
(the second of four round buttons at the bottom of the menu; if you
only see three buttons, you must correctly set up `iio-sensor-proxy`
to detect the accelerometer).

#### Options

* `--auto-lock` / `-a`: Automatically lock the screen orientation when
  docking and unlock it when docking.  If the device is docked when
  the program is started, the screen orientation is locked.  The
  rotation-lock button is disabled while dock.
* `--notify` / `-n`: Display a system notification announcing the
  orientation lock state at launch and every time the setting is
  changed.

### systemd service file (optional)

If you install the included systemd service file, you can have the
program launch automatically in the background when you log in.
Simply enter the following command:

    $ systemctl --user enable helix-orientation-lock.service

This only needs to be done once.

By default, the service file uses the `--auto-lock` option (only).  To
change the options, run:

    $ systemctl edit --full --user helix-orientation-lock.service

The edited version can be found in
`$HOME/.config/systemd/user/helix-orientation-lock.d/override.conf`
