/* 
 * helix-orientation-lock.c --- 
 * 
 * Copyright (C) 2018 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <stdbool.h>
#include <stdlib.h>
#include <config.h>
#include <signal.h>
#include <poll.h>
#include <glob.h>
#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <argp.h>
#include <libevdev/libevdev.h>
#include <gio/gio.h>
#include <libnotify/notify.h>
#include <libnotify/notification.h>

const char *program_name = PACKAGE_NAME;
const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;
static char doc[] =
  "helix-orientation-lock -- A simple daemon to handle screen orientation-locking"
  " on the Lenovo ThinkPad Helix 2nd Gen computer";
static char args_doc[] = "";

static struct argp_option options[] = {
  {"notify", 'n', 0, 0, "Show a notification when the orientation lock status"
   " changes", 0},
  {"auto-lock", 'a', 0, 0, "Automatically lock the orientation when docking and"
   " unlock when undocking", 0},
  {0, 0, 0, 0, 0, 0}
};

struct arguments
{
  bool notify;
  bool auto_lock;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'a':
      arguments->auto_lock = true;
      break;
    case 'n':
      arguments->notify = true;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc,
                           (const struct argp_child *) NULL,
                           NULL, (const char *) NULL};

int
grab_input_dev ()
{
  struct libevdev *input_dev;
  glob_t event_glob;
  int err;
  unsigned int i;
  int fd;
  err = glob ("/dev/input/event*", GLOB_ERR | GLOB_NOSORT, NULL, &event_glob);
  if (err)
    {
      if (err == GLOB_ABORTED)
        {
          globfree (&event_glob);
          error (EXIT_FAILURE, errno, "Failed to glob the /dev/input directory");
        }
      if (err == GLOB_NOMATCH)
        {
          globfree (&event_glob);
          error (EXIT_FAILURE, errno, "No devices found in /dev/input");
        }
      if (err == GLOB_NOSPACE)
        {
          globfree (&event_glob);
          error (EXIT_FAILURE, errno, "Memory exhausted while globbing /dev/input");
        }
    }
  for (i=0; i<event_glob.gl_pathc; i++)
    {
      fd = open (event_glob.gl_pathv[i], O_RDONLY);
      if (fd < 0)
        {
          error (0, errno, "Failed to open %s", event_glob.gl_pathv[i]);
          continue;
        }
      err = libevdev_new_from_fd (fd, &input_dev);
      if (err)
        {
          error (0, 0, "Failed to initialize an evdev construct for %s",
                 event_glob.gl_pathv[i]);
          continue;
        }
      if (!strcmp (libevdev_get_name (input_dev), "Intel Virtual Button driver"))
        {
          libevdev_free (input_dev);
          globfree (&event_glob);
          return fd;
        }
      libevdev_free (input_dev);
      close (fd);
    }
  globfree (&event_glob);
  return -1;
}

void
send_notification (NotifyNotification *notification, gboolean is_locked_val)
{
  char *msg;
  char *icon;
  if (is_locked_val)
    {
      msg = "Screen orientation locked";
      icon = "rotation-locked-symbolic";
    }
  else
    {
      msg = "Screen orientation unlocked";
      icon = "rotation-allowed-symbolic";
    }
  if (notification == NULL)
    {
      notification = notify_notification_new (msg, NULL, icon);
      notify_notification_set_app_name (notification, NULL);
      notify_notification_set_category (notification, "device");
      notify_notification_set_timeout (notification, NOTIFY_EXPIRES_DEFAULT);
      notify_notification_set_urgency (notification, NOTIFY_URGENCY_LOW);
    }
  else
    {
      notify_notification_update (notification, msg, NULL, icon);
    }
  if (!notify_notification_show (notification, NULL))
    {
        error (0, 0, "Failed to send notification");
    }
}

void
set_orientation_lock (GSettings *settings, NotifyNotification *notification,
                      bool value, bool notify)
{
  g_settings_set_boolean (settings, "orientation-lock", value);
  if (notify)
    send_notification (notification, value);
  g_settings_sync ();
}

void
handle_event (struct libevdev *input_dev, struct input_event ev,
              GSettings *settings, NotifyNotification *notification,
              bool notify, bool auto_lock, int tablet_mode)
{
  GVariant *is_locked;
  gboolean is_locked_val;
  if (auto_lock && ev.type == EV_SW && ev.code == SW_TABLET_MODE)
    {
      set_orientation_lock (settings, notification, !tablet_mode, notify);
    }
  else if (ev.type == EV_KEY && ev.code == 561 && ev.value == 1 &&
           (tablet_mode || !auto_lock))
    {
      /* rotation lock button */
      is_locked = g_settings_get_value (settings, "orientation-lock");
      is_locked_val = g_variant_get_boolean (is_locked);
      set_orientation_lock (settings, notification, !is_locked_val, notify);
    }
  return;
}

void
resync_events (struct libevdev *input_dev, GSettings *settings,
               NotifyNotification *notification, bool notify, bool auto_lock)
{
  struct input_event ev;
  int tablet_mode;
  int status = LIBEVDEV_READ_STATUS_SYNC;
  while (status == LIBEVDEV_READ_STATUS_SYNC)
    {
      status = libevdev_next_event (input_dev, LIBEVDEV_READ_FLAG_SYNC, &ev);
      tablet_mode = libevdev_get_event_value (input_dev, EV_SW, SW_TABLET_MODE);
      if (status < 0)
        {
          if (status != -EAGAIN)
            {
              error (0, errno, "Failed grabbing next event: %s",
                     strerror (status));
            }
          error (0, 0, "State change since SYN_DROPPED: %s %s %d\n",
                   libevdev_event_type_get_name (ev.type),
                   libevdev_event_code_get_name (ev.type, ev.code),
                   ev.value);
          return;
        }
      else if (status == LIBEVDEV_READ_STATUS_SUCCESS)
        {
          handle_event (input_dev, ev, settings, notification, notify,
                        auto_lock, tablet_mode);
        }
    }
}

void
main_loop (int ivb_fd, bool notify, bool auto_lock)
{
  struct input_event ev;
  struct libevdev *input_dev;
  int status, err;
  struct pollfd fds[1];
  GSettings *settings = g_settings_new
    ("org.gnome.settings-daemon.peripherals.touchscreen");
  GVariant *is_locked;
  gboolean is_locked_val;
  NotifyNotification *notification;
  int tablet_mode;

  fds[0].fd = ivb_fd;
  fds[0].events = POLLIN;
  err = libevdev_new_from_fd (ivb_fd, &input_dev);
  if (err)
    {
      error (EXIT_FAILURE, 0, "Failed to create an evdev context for the Intel"
             " Virtual Button device");
    }
  tablet_mode = libevdev_get_event_value (input_dev, EV_SW, SW_TABLET_MODE);
  if (!tablet_mode && auto_lock)
    {
      /* We're starting out docked and the dock-lock 
         lock the orientation */
      set_orientation_lock (settings, notification, true, notify);
    }
  else
    {
      /* Output the current lock state */
      is_locked = g_settings_get_value (settings, "orientation-lock");
      is_locked_val = g_variant_get_boolean (is_locked);
      if (notify)
        send_notification (notification, is_locked_val);
    }

  while (true)
    {
      while (!poll (fds, 1, -1)) 0;
      do
        {
          status = libevdev_next_event
            (input_dev, LIBEVDEV_READ_FLAG_NORMAL|LIBEVDEV_READ_FLAG_BLOCKING, &ev);
          tablet_mode = libevdev_get_event_value (input_dev, EV_SW, SW_TABLET_MODE);
          if (status < 0)
            {
              if (status != -EAGAIN)
                {
                  error (0, errno, "Failed to grab the next event: %s",
                         strerror (status));
                }
            }
          else if (status == LIBEVDEV_READ_STATUS_SYNC)
            {
              resync_events (input_dev, settings, notification, notify,
                             auto_lock);
            }
          else if (status == LIBEVDEV_READ_STATUS_SUCCESS)
            {
              handle_event (input_dev, ev, settings, notification, notify,
                            auto_lock, tablet_mode);
            }
        }
      while (status == LIBEVDEV_READ_STATUS_SYNC ||
             status == LIBEVDEV_READ_STATUS_SUCCESS ||
             status == -EAGAIN);
    }
  g_object_unref (G_OBJECT(settings));
  g_object_unref (G_OBJECT(is_locked));
  if (notify)
    g_object_unref (G_OBJECT(notification));
  libevdev_free (input_dev);
  return;
}

int
main (int argc, char **argv)
{
  struct arguments args;
  error_t argp_result;
  int ivb_fd;

  args.notify = false;
  args.auto_lock = false;
  argp_result = argp_parse (&argp, argc, argv, 0, 0, &args);
  if (argp_result)
    error (EXIT_FAILURE, argp_result, "Failed to parse arguments");

  ivb_fd = grab_input_dev ();
  if (ivb_fd < 0)
    error (EXIT_FAILURE, 0, "Failed to find the Intel Virtual Button input");
  notify_init ("ThinkPad Helix 2 Orientation Lock Daemon");
  main_loop (ivb_fd, args.notify, args.auto_lock);
  close (ivb_fd);
  notify_uninit ();
  exit (EXIT_SUCCESS);
}
